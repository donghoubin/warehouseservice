package com.business.warehouseService.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @Description:
 * @Author: Mike Dong
 * @Date: 2019/6/6 14:34.
 */
@Entity
@Table(name = "warehouse_product")
public class WareHouseProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wp_id", nullable = false, length = 10)
    private Long wpId;

    @Column(name = "product_id", nullable = false, length = 10)
    private Long productId;

    @Column(name = "w_id", nullable = false, length = 5)
    private Integer wId;

    @Column(name = "current_cnt", nullable = false, length = 10)
    private Long currentCnt;

    @Column(name = "lock_cnt", nullable = false, length = 10)
    private Long lockCnt;

    @Column(name = "in_transit_cnt", nullable = false, length = 10)
    private Long inTransitCnt;

    @Column(name = "average_cost", nullable = false, precision = 8, scale = 2)
    private BigDecimal averageCost;

    @Column(name="modified_time", nullable = false)
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp modifiedTime;

    public Long getWpId() {
        return wpId;
    }

    public void setWpId(Long wpId) {
        this.wpId = wpId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getwId() {
        return wId;
    }

    public void setwId(Integer wId) {
        this.wId = wId;
    }

    public Long getCurrentCnt() {
        return currentCnt;
    }

    public void setCurrentCnt(Long currentCnt) {
        this.currentCnt = currentCnt;
    }

    public Long getLockCnt() {
        return lockCnt;
    }

    public void setLockCnt(Long lockCnt) {
        this.lockCnt = lockCnt;
    }

    public Long getInTransitCnt() {
        return inTransitCnt;
    }

    public void setInTransitCnt(Long inTransitCnt) {
        this.inTransitCnt = inTransitCnt;
    }

    public BigDecimal getAverageCost() {
        return averageCost;
    }

    public void setAverageCost(BigDecimal averageCost) {
        this.averageCost = averageCost;
    }

    public Timestamp getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Timestamp modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
