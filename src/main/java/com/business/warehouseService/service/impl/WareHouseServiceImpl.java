package com.business.warehouseService.service.impl;

import com.business.warehouseService.entity.WareHouseProduct;
import com.business.warehouseService.repository.WareHouseRepository;
import com.business.warehouseService.service.WareHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @Description:
 * @Author: Mike Dong
 * @Date: 2019/6/6 14:35.
 */
@Service
public class WareHouseServiceImpl implements WareHouseService {

    @Autowired
    private WareHouseRepository wareHouseRepository;

    @Override
    public void addWareHouseProduct(WareHouseProduct wareHouseProduct) {
        wareHouseProduct.setModifiedTime(new Timestamp(new Date().getTime()));
        wareHouseRepository.save(wareHouseProduct);
    }

    /**
     * @Description: Deduct warehouse product.
     * @Author: Mike Dong
     * @Date: 2019/6/6 15:14
     * @Param: [productId, deductCnt]
     * @Return: void
     */
    @Override
    public void deductWareHouseProduct(Long productId, Long deductCnt) {
        wareHouseRepository.deductWareHouseProduct(productId, deductCnt);
    }
}
