package com.business.warehouseService.service;

import com.business.warehouseService.entity.WareHouseProduct;

/**
 * @Description:
 * @Author: Mike Dong
 * @Date: 2019/6/6 14:35.
 */
public interface WareHouseService {

    void addWareHouseProduct(WareHouseProduct wareHouseProduct);

    void deductWareHouseProduct(Long productId, Long deductCnt);
}
