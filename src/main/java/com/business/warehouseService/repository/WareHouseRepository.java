package com.business.warehouseService.repository;

import com.business.warehouseService.entity.WareHouseProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description:
 * @Author: Mike Dong
 * @Date: 2019/6/6 14:34.
 */
@Repository
public interface WareHouseRepository extends JpaRepository<WareHouseProduct, Long> {

    @Transactional
    @Query("update WareHouseProduct set currentCnt = currentCnt-?2 where productId = ?1")
    @Modifying
    void deductWareHouseProduct(Long productId, Long deductCnt);
}
