package com.business.warehouseService.controller;

import com.business.warehouseService.entity.WareHouseProduct;
import com.business.warehouseService.service.WareHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Mike Dong
 * @Date: 2019/6/6 14:34.
 */
@RestController
public class WareHouseController {

    @Autowired
    private WareHouseService wareHouseService;

    /*
     * @Description: add warehouse product.
     * @Author:      Mike Dong
     * @Date:        2019/6/6 15:46
     * @Param:       [wareHouseProduct]
     * @Return:      void
     */
    @PostMapping(path="/addWareHouseProduct")
    public void addWareHouseProduct(@RequestBody WareHouseProduct wareHouseProduct) {
        wareHouseService.addWareHouseProduct(wareHouseProduct);
    }

    /**
     * @Description: Deduct warehouse product.
     * @Author: Mike Dong
     * @Date: 2019/6/6 15:12
     * @Param: [productId, deductCnt]
     * @Return: void
     */
    @PostMapping(path = "/deductWareHouseProduct")
    public void deductWareHouseProduct(@RequestParam("productId") Long productId, @RequestParam("deductCnt") Long deductCnt) {
        wareHouseService.deductWareHouseProduct(productId, deductCnt);
    }
}
